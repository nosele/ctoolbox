This repository contains full code and code snippets from around the web which are interesting or useful to me.

Some may have quirks to compile, they will be documented presently:

halftone.c requires -std=c99 to compile

Content Description

halftone.c is a cli program that converst RGB images to grayscale halftone images that imitate newspaper print.

Attribution or Sources

halftone.c was written by skeeto for the [Easy] Daily Programing Challenge #179 "You make me happy when clouds are gray...scale"  on Reddit's /r/dailygramming 
subreddit. http://www.reddit.com/r/dailyprogrammer/comments/2ftcb8//ckcm49f
